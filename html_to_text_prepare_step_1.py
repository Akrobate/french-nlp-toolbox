#
# This script takes raw html files and export a bunch of json files
# One file per html folder, one json property per formatted text
#

from libraries.source_html_files import SourceHtmlFiles
from libraries.html_text_extractor import HtmlTextExtractor


html_files = SourceHtmlFiles()
text_extractor = HtmlTextExtractor()


# Seting configurations
html_files.setSourceRawFilesDirectory('data/raw_html_sources/')
html_files.setOutputPath('data/prepared_sources/')

# Full processing, the processor function is injected
html_files.process_all(text_extractor.process_text)
