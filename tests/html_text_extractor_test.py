import os

from html_text_extractor import HtmlTextExtractor

seed_path = './tests/data/raw_html_data/61792'
home_page_filepath = os.path.join(seed_path, 'home.html')

def test_testing_framework():
    # check values
    assert True


def strip_ponctuation_test():
    text_with_ponctuation = 'Hello, world!'
    extractor = HtmlTextExtractor()
    result = extractor.strip_ponctuation(text_with_ponctuation)
    assert result == 'Hello world'


def only_letters_test():
    text_with_ponctuation = '123 Hello, world! @char 4120,455 ok ?'
    extractor = HtmlTextExtractor()
    result = extractor.only_letters(text_with_ponctuation)
    assert result == 'Hello world char ok'


def format_html_file_test():
    file_handler = open(home_page_filepath, 'r', encoding="utf8")
    raw_html_string = file_handler.read()
    file_handler.close()

    extractor = HtmlTextExtractor()
    text = extractor.text_from_html(raw_html_string)

    assert len(text) > 0

def process_text_test():
    file_handler = open(home_page_filepath, 'r', encoding="utf8")
    raw_html_string = file_handler.read()
    file_handler.close()

    extractor = HtmlTextExtractor()
    text = extractor.process_text(raw_html_string)

    assert len(text) > 0
