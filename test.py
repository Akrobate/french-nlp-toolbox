# -*- coding: utf-8 -*-

import re
import glob
import os
import json
import string

from bs4 import BeautifulSoup
from bs4.element import Comment
from unicodedata import category
from langdetect import detect


file_path = 'D:/crawler/raw_data/'
result_file_path = 'D:/crawler/structured_data/'

def tag_visible(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
        return False
    if isinstance(element, Comment):
        return False
    return True


def text_from_html(body):
    soup = BeautifulSoup(body, 'html.parser')
    texts = soup.findAll(text=True)
    visible_texts = filter(tag_visible, texts)  
    return u" ".join(t.strip() for t in visible_texts)


def strip_ponctuation(text):
    text_with_striped_ascii_poncutuation = text.translate(text.maketrans(string.punctuation, ' ' * len(string.punctuation)))
    # text_with_utf8_stripped_ponctuation = ''.join(ch for ch in text_with_striped_ascii_poncutuation if category(ch)[0] != 'P')    
    text_with_utf8_stripped_ponctuation = ''.join(ch if category(ch)[0] != 'P' else ' ' for ch in text_with_striped_ascii_poncutuation)
    return text_with_utf8_stripped_ponctuation


def only_letters(text):
    return re.sub("(\\d|\\W)+"," ", text)

def is_french(text):
    return detect(text) == 'fr'

def count_words(text):
    return len(text.split(' '))


def open_directory_to_process(directory_id, default_path):
    filename_list = glob.glob(default_path + str(directory_id) + '/*.html')
    # print(filename_list)
    result = {}
    for filename in filename_list:
        base = os.path.basename(filename)
        file = os.path.splitext(base)[0]
        result[file] = filename
    return result

def process_directory(directory_id, default_path):
    files_to_process = open_directory_to_process(directory_id, default_path)
    result = {}
    for index, value in files_to_process.items():
        text_file_handler = open(value, 'r', encoding="utf8")
        text = text_file_handler.read()
        text_file_handler.close()
        text = text_from_html(text)
        text = strip_ponctuation(text)
        text = only_letters(text)
        result[index] = text
        print('.', end = '')
    print('ok')
    return result



def process_all(input_path, output_path):
    directories = glob.glob(input_path + '*')
    allready_processed = all_ready_processed_directories_list(output_path)
    for directory in directories:
        base = os.path.basename(directory)
        directory_name = os.path.splitext(base)[0]
        if directory_name not in allready_processed:
            print('Processing: ' + str(directory_name))
            result = process_directory(directory_name, input_path)
            with open(output_path + str(directory_name) + '.json','w', encoding="utf-8") as file: 
                file.write(json.dumps(result)) 
                file.close()
        else:
            print('Allready processed: ' + str(directory_name))


def all_ready_processed_directories_list(output_path):
    directories = glob.glob(output_path + '*')
    result = ['47286', '53016', '55002', '6680']
    for directory in directories:
        base = os.path.basename(directory)
        directory_name = os.path.splitext(base)[0]
        result.append(directory_name)
    return result


process_all(file_path, result_file_path)
