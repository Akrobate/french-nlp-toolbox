# French NLP Toolbox

This collection of script permit to the user to process raw html data for text mining methods.

First of all the script

## Raw html to clean texts 

- **html_to_text_prepare.py** script will process a collection of folders in order to extract and normalize visibile texts


## Steps for text analysis and topics extractions

- **step_1_prepare_source_data.py**
- **step_2_transform_text_data.py**
- **Step_3_topic_generation**
    - **step_3_LDA_topic_generation.py**
    - **step_3_NMF_topics_generation**


### step_1_prepare_source_data.py

Takes as input a csv file with columns: id, activity_description, active. Filter lines only on lines marked with active to true. Filter out all lines with activity_description with less then 5 characteres. Removes the out liers. Lematizes the activity_description field. Adds a field with activity description char lengths. Provides an outout in csv file: **step_1_result_texts_lema.csv**

### step_2_transform_text_data.py

Transforms text data to two models. TfIdf model, and the text sparsed matrix. Both files a stored in joblib format. **step_2_tfidf_model.joblib** and **step_2_csr_matrix.joblib**. This script can be fine tuned to accept some input params **ngram_min**, **ngram_max** for the tfidf model.

### step_3_[LDA|NMF]_topic_generation.py

Uses as input the **step_2_csr_matrix.joblib** and train model LDA or NMF. Then applies the model on the csr_matrix. This script will output the model saved as **step_3_lda_n_components_c_model.joblib** and the result of model applied to csr_matrix sanved as **step_3_lda_n_components_c_features.joblib**. This script uses n_component param from configuration that defines the wanted number of topics

### analyse_step_1.py

Provide lengths of contents analysis. This scripts draws a violon plot to preview how data is distributed.

### analyse_step_2.py

Topics extraction previewer. Once all steps of topics extraction processing is done, this script will render the found topics. Each topic is a words collection ordered from the most representative word for the topic to the less representative word

### html_to_text_prepare_step_1.py

This script process the source files generated by the the https://github.com/Akrobate/website-text-downloader-python. It will generate a bunch of json files with texts cleaned.


## Installation and usage

### CPU Version
```bash
#!/bin/bash

DOCKER_IMAGE_NAME="french-nlp-CPU"
PORT="8080"

COMMAND_START_JUPYTER="conda run --name python36 jupyter notebook --ip=0.0.0.0 --port=$PORT --allow-root"

docker build -t $DOCKER_IMAGE_NAME ./dockerfile/cpu/
docker run --rm -p $PORT:$PORT -v `pwd`/data:/data -v `pwd`/.jupyter:/root/.jupyter -i -t $DOCKER_IMAGE_NAME bash
```

### GPU Version
```bash
#!/bin/bash

DOCKER_IMAGE_NAME="french-nlp-GPU"
PORT="8080"

COMMAND_START_JUPYTER="conda run --name python36 jupyter notebook --ip=0.0.0.0 --port=$PORT --allow-root"

docker build -t $DOCKER_IMAGE_NAME ./dockerfile/gpu/
docker run --rm --gpus all -p $PORT:$PORT -v `pwd`/data:/data -v `pwd`/.jupyter:/root/.jupyter -i -t $DOCKER_IMAGE_NAME bash
```

### Docker GPU vs Docker CPU

Two different images can be build

The default **Dockerfile** install the GPU Support for Nvidia GPU. It also install specific package with gpu support. For instance Spacy text processor librairy is the GPU version installer

The alternative image **Dockerfile_CPU** is a CPU version without gpu support. This version should directly works on any docker host. 



