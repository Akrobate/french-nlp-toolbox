import glob
import os
import json

file_path = 'D:/crawler/raw_data/'
result_file_path = 'D:/crawler/structured_data/'

class SourceHtmlFiles:

    def __init__(self):
        self.file_path = ''
        self.result_file_path = ''

    def setSourceRawFilesDirectory(self, path):
        self.file_path = path
    
    def setOutputPath(self, path):
        self.result_file_path = path

    # Maybe processor method should be stored in class    
    def process_all(self, processor_method):

        input_path = self.file_path
        output_path = self.result_file_path

        directories = glob.glob(input_path + '*')
        allready_processed = self.all_ready_processed_directories_list(output_path)
        for directory in directories:
            base = os.path.basename(directory)
            directory_name = os.path.splitext(base)[0]
            if directory_name not in allready_processed:
                print('Processing: ' + str(directory_name))
                result = self.process_directory(directory_name, input_path, processor_method)
                self.saveResultFile(directory_name, result)
            else:
                print('Allready processed: ' + str(directory_name))

    def process_directory(self, directory_id, default_path, processor_method):
        files_to_process = self.open_directory_to_process(directory_id, default_path)
        result = {}
        for index, value in files_to_process.items():
            text = self.readFileToProcess(value)
            processor_method(text)
            result[index] = text

        return result

    def all_ready_processed_directories_list(self, output_path):
        directories = glob.glob(output_path + '*')
        result = []
        for directory in directories:
            base = os.path.basename(directory)
            directory_name = os.path.splitext(base)[0]
            result.append(directory_name)
        return result

    def open_directory_to_process(self, directory_id, default_path):
        filename_list = glob.glob(default_path + str(directory_id) + '/*.html')
        result = {}
        for filename in filename_list:
            base = os.path.basename(filename)
            file = os.path.splitext(base)[0]
            result[file] = filename
        return result  

    def saveResultFile(self, directory_name, result):
        output_result_file_path = os.path.join(self.output_path, str(directory_name) + '.json')
        with open(output_result_file_path, 'w', encoding="utf-8") as file: 
            file.write(json.dumps(result)) 
            file.close()
    
    def readFileToProcess(self, file_path):
        file_handler = open(file_path, 'r', encoding="utf8")
        text = file_handler.read()
        file_handler.close()
        return text
