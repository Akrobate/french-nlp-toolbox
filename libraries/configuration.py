import configparser

config = configparser.ConfigParser()
config.read('configuration.ini')

steps_path = config['PATH']['StepsPath']
source_file = config['INPUT']['SourceFile']

ngram_min = config['NGRAM']['Min']
ngram_max = config['NGRAM']['Max']

n_components = config['TOPICS']['Number']
