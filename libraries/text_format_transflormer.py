# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-

import spacy

class TextFormatTransformer:
    
    def __init__(self, pos_list = ['NOUN', 'VERB', 'ADJ'], spacy_core = 'fr_core_news_md'):
        self.nlp = spacy.load(spacy_core)
        self.pos_list = pos_list
    
    
    def fit_transform(self, text_list):
        text_list_result = text_list.copy()
        text_list_result = text_list_result.fillna('')
        text_list_result = text_list_result.apply(self.selectWordAndLematize)        
        text_list_result = text_list_result.fillna('')
        return text_list_result
        
        
    def selectWordAndLematize(self, text):
        list_result = []
        for token in self.nlp(text):
            accpeted_pos = ['NOUN', 'VERB', 'ADJ']
            if (token.pos_ in accpeted_pos and token.is_alpha):
                list_result.append(token.lemma_)
        return " ".join(list_result)


    def length_count(self, text_list):
        text_list_result = text_list.copy()
        text_list_result = text_list_result.fillna('')
        return text_list_result.apply(lambda x : len(x))
        