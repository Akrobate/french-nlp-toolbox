# -*- coding: utf-8 -*-

import re
import json
import string

from bs4 import BeautifulSoup
from bs4.element import Comment
from unicodedata import category


class HtmlTextExtractor:

    def process_text(self, text):
        result = self.text_from_html(text)
        result = self.strip_ponctuation(result)
        result = self.only_letters(result)
        return result

    def text_from_html(self, body):
        soup = BeautifulSoup(body, 'html.parser')
        texts = soup.findAll(text=True)
        visible_texts = filter(self.tag_visible, texts)  
        return " ".join(t.strip() for t in visible_texts)

    def strip_ponctuation(self, text):
        text_with_striped_ascii_poncutuation = text.translate(text.maketrans(string.punctuation, ' ' * len(string.punctuation)))
        # text_with_utf8_stripped_ponctuation = ''.join(ch for ch in text_with_striped_ascii_poncutuation if category(ch)[0] != 'P')    
        text_with_utf8_stripped_ponctuation = ''.join(ch if category(ch)[0] != 'P' else ' ' for ch in text_with_striped_ascii_poncutuation)
        return text_with_utf8_stripped_ponctuation

    def only_letters(self, text):
        return re.sub("(\\d|\\W)+"," ", text)

    def tag_visible(self, element):
        if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
            return False
        if isinstance(element, Comment):
            return False
        return True

    def count_words(self, text):
        return len(text.split(' '))
