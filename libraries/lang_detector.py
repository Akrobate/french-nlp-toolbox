from langdetect import detect

class LangDetector:

    def is_french(self, text):
        return self.detect(text)        

    def detect(self, text):
        return detect(text) == 'fr'
