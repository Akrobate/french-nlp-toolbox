import os
from joblib import load 
from libraries.configuration import steps_path, n_components

def print_top_words(model, feature_names, n_top_words):
    results = []
    for topic_idx, topic in enumerate(model.components_):
        message = "Topic #%d: " % topic_idx
        message += " ".join([feature_names[i]
                             for i in topic.argsort()[:-n_top_words - 1:-1]])
        results.append(message)
    return results


tfidf = load(os.path.join(steps_path, 'step_2_tfidf_model.joblib'))
model = load(os.path.join(steps_path, 'step_3_nmf_' + str(n_components) + 'c_features.joblib'))

words = tfidf.get_feature_names()

top_words = print_top_words(model, words, 20)

# Should be stored in steps results
print(top_words)
