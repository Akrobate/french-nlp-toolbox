# -*- coding: utf-8 -*-

import os
import pandas as pd
from configuration import steps_path, source_file

from libraries.text_format_transformer import TextFormatTransformer

tft = TextFormatTransformer()

# Loading source file
source_df = pd.read_csv(source_file, index_col='id', usecols=['id', 'active', 'activity_description'])
source_df = source_df[source_df.active == 1].copy()

# And add length column
source_df['activity_description_length'] = tft.length_count(source_df['activity_description'])

# Remove all data without descriptions
source_df = source_df[source_df['activity_description_length'] > 5].copy()

# Remove the max outliers
q_hi = source_df['activity_description_length'].quantile(0.99995)
source_df = source_df[source_df['activity_description_length'] < q_hi].copy()

# Lemmatize
source_df['text'] = tft.fit_transform(source_df['activity_description'])
source_df['text_length'] = tft.length_count(source_df['text'])

source_df.to_csv(os.path.join(steps_path, 'step_1_result_texts_lema.csv'), index=True)

