# -*- coding: utf-8 -*-
import os
import pandas as pd
from joblib import dump
from sklearn.feature_extraction.text import TfidfVectorizer
import seaborn as sns
from libraries.configuration import steps_path

source_df = pd.read_csv(os.path.join(steps_path, 'step_1_result_texts_lema.csv'), index_col=['id'])

sns.violinplot(y=source_df['activity_description_length'])
