import os
from joblib import dump, load
from sklearn.decomposition import LatentDirichletAllocation
from configuration import steps_path, n_components

csr_matrix = load(os.path.join(steps_path, 'step_2_csr_matrix.joblib'))

lda_model = LatentDirichletAllocation(n_components = n_components, verbose=True)
lda_model.fit(csr_matrix)
lda_features = lda_model.transform(csr_matrix)

dump(
    lda_model,
    os.path.join(steps_path, 'step_3_lda_' + str(n_components) + 'c_model.joblib')
)

dump(
    lda_features,
    os.path.join(steps_path, 'step_3_lda_' + str(n_components) + 'c_features.joblib')
)
