# -*- coding: utf-8 -*-
import os
import pandas as pd
from joblib import dump
from sklearn.feature_extraction.text import TfidfVectorizer
from configuration import steps_path, ngram_min, ngram_max

texts_lema = pd.read_csv(os.path.join(steps_path, 'step_1_result_texts_lema.csv'), index_col=['id'])
texts_lema = texts_lema[texts_lema['text_length'] > 0].copy()

# stop_words=stop_words
tfidf = TfidfVectorizer(strip_accents='unicode', ngram_range=(ngram_min, ngram_max))
csr_matrix = tfidf.fit_transform(texts_lema['text'])

dump(
    tfidf,
    os.path.join(steps_path, 'step_2_tfidf_model.joblib')
)

dump(
    csr_matrix,
    os.path.join(steps_path, 'step_2_csr_matrix.joblib')
)
