import os
from joblib import dump, load
from sklearn.decomposition import NMF
from configuration import steps_path, n_components

csr_matrix = load(os.path.join(steps_path, 'step_2_csr_matrix.joblib'))

nmf_model = NMF(n_components = n_components, verbose=True)
nmf_model.fit(csr_matrix)
nmf_features = nmf_model.transform(csr_matrix)

dump(
    nmf_model,
    os.path.join(steps_path, 'step_3_nmf_' + str(n_components) + 'c_model.joblib')
)

dump(
    nmf_features,
    os.path.join(steps_path, 'step_3_nmf_' + str(n_components) + 'c_features.joblib')
)
